class Fraccion:
    def __init__(self, num = 0.0, den = 1.0):  #qué tiene, por qué tiene eso
        self.numerador = num
        self.denominador = den

    def suma(self, otro):
        self.numerador = self.numerador *  otro.denominador + self.denominador*otro.numerador
        self.denominador = self.denominador * otro.denominador
        return self

    def sumar(izq, dcha):
        return Fraccion(izq.numerador*dcha.denominador + dcha.numerador*izq.denominador )

    def resta(self, otro):
        self.numerador = self.numerador *  otro.denominador - self.denominador*otro.numerador
        self.denominador = self.denominador * otro.denominador
        return self

    def multiplicacion(self, otro):
        self.numerador = self.numerador * otro.numerador
        self.denominador = self.denominador * otro.denominador
        return self

    def division(self, otro):
        self.numerador = self.numerador * otro.denominador
        self.denominador = self.denominador * otro.numerador
        return self

   




unmedio = Fraccion (1, 2)
uno = Fraccion(1)
cero = Fraccion ()

#misuma = uno + unmedio
misuma = uno.suma(unmedio) #me cambio a mi mismo porque me he ido sumando cosas y obteniendo numeradores y denominadores distintos al del principio
#misuma = Fraccion.sumar(uno, unmedio)
print(misuma)