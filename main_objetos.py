class Empleado: #puede tener diferentes tipos de cosillas
    def __init__(self, nombre, salario, tasa, antiguedad): #self son los atributos que tiene el empleado
        self.__nombre = nombre #siempre que se cree init es para inicializar los atributos
        self.__salario = salario #las dos líneas abajo, hacen que sea privado, si no podríamos haber accedido directamente
        self.__tasa = tasa
        self.__antiguedad = antiguedad

    def CalculoImpuestos(self):
        self.__impuestos = self.__salario*self.__tasa
        print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.__nombre, tax=self.__impuestos))
        return self.__impuestos
    def ImprimirEdad(self):
        print("Antigüedad de: " + self.__nombre + "es de: " + str(self.__antiguedad))
    def DeclararAntiguedad(self):
        print("Este empleado: " + self.__nombre + " lleva estos años de antigüedad: " + str(self.__antiguedad))
        if self.__antiguedad > 1:
            print("Con este empleado: " + self.__nombre + " la empresa se ahorra un 10%.")

def displayCost(total):
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))


emp1 = Empleado("Pepe", 20000, 0.35, 1)
emp2 = Empleado("Ana", 30000, 0.30, 20)
emp1.ImprimirEdad()
empleados = [emp1, emp2, Empleado("Luis", 10000, 0.10, 15), Empleado("Luisa", 25000, 0.15, 14)]
total = 0
for emp in empleados:
    emp.DeclararAntiguedad()
    total += emp.CalculoImpuestos()
displayCost(total)